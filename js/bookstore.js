var BlueBookStore = window.BlueBookStore || {};

(function BookStoreScopeWrapper($) {
    var authToken;

    BlueBookStore.authToken.then(function setAuthToken(token) {
        if (token) {
            authToken = token;
            displayUpdate(' ' +token);
        } else {
            window.location.href = '/signin.html';
        }
    }).catch(function handleTokenError(error) {
        alert(error);
        window.location.href = '/signin.html';
    });


    function displayUpdate(text) {
        $('#updates').append(text);
    }


}(jQuery));
